#!/bin/bash

source .env

docker tag "$IMAGE_NAME:$TAG" "$REGISTRY_URL$IMAGE_NAME:$TAG"
docker push "$REGISTRY_URL$IMAGE_NAME:$TAG"