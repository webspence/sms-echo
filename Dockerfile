FROM python:3.10.0a4-alpine3.12

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

ENV PORT=5001 \
  TWILIO_ACCOUNT_ID= TWILIO_AUTH_TOKEN= FROM_PHONE_NUMBER= ACCESS_TOKEN=

EXPOSE $PORT

HEALTHCHECK --interval=30s --timeout=10s \
  CMD curl -f http://localhost:$PORT/health || exit 1

CMD [ "python", "./main.py" ]