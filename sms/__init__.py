import os
# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client

TWILIO_ACCOUNT_ID = os.environ["TWILIO_ACCOUNT_ID"] if "TWILIO_ACCOUNT_ID" in os.environ else ""
TWILIO_AUTH_TOKEN = os.environ["TWILIO_AUTH_TOKEN"] if "TWILIO_AUTH_TOKEN" in os.environ else ""
FROM_PHONE_NUMBER = os.environ["FROM_PHONE_NUMBER"] if "FROM_PHONE_NUMBER" in os.environ else ""


def send_sms(recipientPhoneNumber: str, message: str):
    # Your Account Sid and Auth Token from twilio.com/console
    account_sid = TWILIO_ACCOUNT_ID
    auth_token = TWILIO_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    message = client.messages \
                    .create(
                        body=message,
                        from_=FROM_PHONE_NUMBER,
                        to=recipientPhoneNumber
                    )

    return message.sid
