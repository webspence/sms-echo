#!/bin/bash
source ../.env

curl -H "Content-Type: application/json" \
  -H "access_token: $ACCESS_TOKEN" \
  --request POST \
  --data "{\"recipientPhoneNumber\": \"$1\", \"message\": \"$2\"}" \
  http://localhost:5001/sms-echo/