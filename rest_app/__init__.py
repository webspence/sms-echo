import os
from functools import wraps
from flask import Flask, request, abort
from flask_cors import CORS

app = Flask(__name__)

DEBUG = True  # TODO: Move to settings object

# Setup cors
origins = []
if DEBUG:
    origins.append('*')

CORS(app)


ACCESS_TOKEN = os.environ["ACCESS_TOKEN"] if "ACCESS_TOKEN" in os.environ else "3cbf321d021aadf9589b8e9d02c25d1bedb44c6cf46c4fec"


def require_access_token(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get("access_token") != ACCESS_TOKEN:
            return abort(401)
        return f(*args, **kwargs)
    return decorated_function
