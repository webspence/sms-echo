from rest_app import app, require_access_token
from flask import request
from sms import send_sms


@app.route("/health/")
def index():
    return "I'm healthy!"


@app.route("/sms-echo/", methods=['POST'])
@require_access_token
def echo_sms():
    response = ""
    json = request.json
    message = json["message"] if json["message"] else "Empty message"
    recipientPhoneNumber = json["recipientPhoneNumber"] if json["recipientPhoneNumber"] else ""

    response = send_sms(recipientPhoneNumber, message)

    return {"response": response}
