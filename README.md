# SMS Echo Server
Echos your message via SMS.

## Deploy to Kubernetes cluster
`helm install sms-echo-server helm-chart`

Next you can apply the manifests (don't forget your `secrets.yaml` file!) in the `/k8s` folder.

## Build Docker image and run container
1. `./scripts/build.sh`
2. `./scripts/run.sh`

## Development

### Setup

Setup the Python virtual environment:

1. `python3 -m venv .venv`
1. `source .venv/bin/activate`
1. `which python` (confirm that the Python interpreter is pointing to .venv)
1. `pip install -r requirements.txt`

