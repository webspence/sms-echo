import os
from rest_app import app
from rest_app import views
from dotenv import load_dotenv

if __name__ == "__main__":
    if os.path.isfile(".env"):
        load_dotenv()

    app.run(debug=True, host="0.0.0.0", port=5001)
